# Comics for Shors Algorithm

**Keywords:** QArt, QComics, QEducation, QJunior

## Motivation

Shor's algorithm has several steps, and each may be repeated.

Describing the overall algorithm as a comic story can make it more friendly for newcomers.

## Video links

Factorization in the Quantum World by Cem Say | https://youtu.be/4LmZIOLM-8s

The Story of Shor's Algorithm, Straight From the Source by Peter Shor | https://youtu.be/6qD9XElTpCE

##

_Prepared by Abuzer Yakaryilmaz_