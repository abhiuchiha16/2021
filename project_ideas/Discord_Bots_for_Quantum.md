# Discord Bots for Quantum

**Keywords:** OpenQEcoSystem, QEducation, QGames, QProgramming

## Motivation

Discord has been started to use in many online quantum events and programs such as workshops, jams, hackathons, courses, internships, etc.

Discord bots can be very helpful for the server management, increasing the interactions, and having more fun.

Growing quantum ecosystem definitely needs several quantum bots for different purposes.

## Some Ideas

A bot to play a simple quantum game.

A bot for taking quantum quizzes (exercises from https://gitlab.com/qworld/bronze-qiskit can be used) indivually.

A bot for playing quantum kahoot games.

A bot for some ice-breaking activities.

A bot for running small quantum circuts.

A bot for networking events.

##

_Prepared by Abuzer Yakaryilmaz_