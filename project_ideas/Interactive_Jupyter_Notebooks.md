# Interactive Jupyter Notebooks

**Keywords:** QEducation, QGames, QFreeAccess, QProgramming, QJunior

## Motivation

Jupyter notebooks are widely used by the quantum ecosystem.

By using JavaScript, more interactive tools can be used.

The additional installation should be minimum, preferable none.

## Some ideas

An easy way to prepare and take a test on the notebooks  
_exercises from https://gitlab.com/qworld/bronze-qiskit can be used_

A simple quantum game working on the notebooks   
_Some example games can be found at https://github.com/HuangJunye/Awesome-Quantum-Games_

A small virtual lab on notebooks  
_An example but advanced platform: https://quantumgame.io/_

A circuit designer on notebooks  
_ref: https://quantumjavascript.app/_  
_ref: https://github.com/quantastica/quantum-circuit_

An easy-to-use tool on the notebooks to run the quantum programs on real devices and then process the outcomes


##

_Prepared by Abuzer Yakaryilmaz_