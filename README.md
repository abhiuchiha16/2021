<img src="/images/qjam2021header.jpg">

Welcome to QWorld's global quantum jam "QJam2021"! An open source quantum event for students, quantum enthusiasts, researchers, educators, teachers, artists, writers, designers, animators, software developers, game developers, programmers, and all alike! 

QJam2021 is held online from Nov 26 (Fri) to Dec 5 (Sun). Before starting, we will host several QJam Talks on Nov 23-25.

###### Website: [https://qworld.net/qjam2021/](https://qworld.net/qjam2021/)

## Registration

Participating in QJam2021 is free, but registration (see below) is mandatory.

[Visit the event page](https://qworld.net/qjam2021/) for registration forms.

## QJam Talks

[Click for the detailed program.](QJamTalks.md)

We are hosting eigth speakers: Marcel Pfaffhauser, Ryuji Takagi, Eduardo Miranda, Réka Deák & Aurél Gábris, Adam Fattal, Nicole Yunger Halpern, and Xavier Coiteux-Roy. Below is the coincise program. 

**Tuesday, Nov 23**  
16:00 – 17:00: Marcel Pfaffhauser

**Wednesday, Nov 24**  
15:00 – 16:00: Ryuji Takagi  
16:00 – 16:50: Eduardo Miranda  
16:50 – 17:20: Réka Deák & Aurél Gábris  
17:20 – 17:40: Adam Fattal

**Thursday, Nov 25**  
17:00 – 18:00: Nicole Yunger Halpern  
18:00 – 18:30: Xavier Coiteux-Roy

## QJam Schedule

The default time zone is UTC.

**Fri (Nov 26), 17:00:** The opening meeting of QJam2021  
**Sat-Sun (Nov 27-28):** Project idea discussions, team formation, and recruitment   
**Mon (Nov 29):** QJamming session | Several time slots will be set  
**Mon-Sat (Nov 29-Dec4):** Working on projects | Our mentors will visit each group regularly   
**Sun (Dec 5), 10:00:** Deadline for project submission & presentation   
**Sun (Dec 5), 15:00:** Deadline for project evaluation   
**Sun (Dec 5), 16:00:** Overview of QJam, completed projects, and closing remarks.  

## Categories (Keywords)

Here is a list of keywords to help for identifying the projects.

_**QEducation, QJunior, QGames, QArt, QComics, QAnimation, QShort, QProgramming, QSoftware, QImplementation, QLibrary, Outreach, QEDI, QFreeAccess QHardware OpenQEcosystem**_


## How to Jam

Each participant is invited to the Discord server dedicated to QJam2021, which is the main communication server. 

We use zoom meetings for the online sessions.

We use this Gitlab repo for project management. All submission guidelines will be present in the guidlines pdf. [Click for the guideline.](guidelines/Guidelines.pdf)

## Licensing

The project details and outcomes developed during QJam2021 should be publicly available, and they should have open-source licenses such as 
[Apache License](http://www.apache.org/licenses/LICENSE-2.0) or 
[MIT license](https://opensource.org/licenses/MIT) or 
[CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/legalcode) or 
[Educational Community License](https://opensource.org/licenses/ECL-2.0).

After QJam2021, the licensing of the new features or developments are up to the project team.

Contact us if you think the nature of your project needs some other types of licensing. We may consider exceptions after evaluating the request within our team.

## Outcomes, evaluation, and awards

We expect each group to create a presentable outcome from their ideas. Outcomes can be in many different forms such as a quantum game, an animation, comics, a short movie, a web application, a smartphone application, a new library, new functionality to jupyter notebooks, a tool for quizzing.

In the end, only the projects with presentable outcomes will be eligible for evaluation, and the members of such projects will get certificates. Both the participants (50%) and the jury (50%) will rate the projects. There will be no competition among the projects. Instead, the members of projects with high scores (out of 10) will get special certificates:
- Certificate of “Excellence” for the projects with scores nine or higher
- Certificate of “Distinction” for the projects with scores between eight and nine
- Certificate of “Merit” for the projects with scores between seven and eight

Evaluation criteria (tentative):
- Motivation (15%)
- Originality (15%)
- Success of implementation (20%)
- Outcome (30%)
- Presentation (20%)

We invite the members of each successful project to continue their work under QWorld until the end of Spring 2022.
- There will be regular (e.g., every two weeks) meetings with project members.
- Each group will have opportunities to interact with QWorld’s members and volunteers. Depending on the availability, we may assign a knowledgeable mentor to your project.
- We plan to have a spring QCourse on research and implementation projects. We may invite suitable projects to this course.
- We help each group to prepare a manuscript or technical report to publish.
- We invite suitable work to be presented at QWorld Quantum Science Days 2022.
- QWorld may integrate some projects into its program based on mutual agreement.

## Code of conduct

[Click for the Code of Conduct of QJam2021.](Code_Of_Conduct.md)


