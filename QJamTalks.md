<img src="/images/qjam2021header.jpg">

As part of the QJam2021 we will host several Talks from November 23 to 25, in order to inspire the participants, below you can review the details of each talk.

To attend, please follow this link: https://us02web.zoom.us/j/88343260805?pwd=TFhLN1BubGdZdEZqLy9tUE9sRVBjdz09

[[_TOC_]]


# Tuesday, Nov 23

## 16:00 – 17:00 (UTC): Marcel Pfaffhauser - **Playing and creating games to learn Quantum Computation**
Talk Recording: https://youtu.be/Fu1UkFaiRsM

Quantum Odyssey Game: https://www.quarksinteractive.com/

**Bio**: Marcel studied computer science at ETH Zurich, with specialization in theory of computing. He worked as a Unity Developer for 5 years, where he was focused on AR VR and Gamification. Last Year he joined IBM Research in Zurich to work in the Quantum Education Team together with James Wootton.

<img src="/images/mp.jpg" width="160px">

# Wednesday, Nov 24

## 15:00 – 16:00 (UTC): Ryuji Takagi - **Fundamental limits of quantum error mitigation**
Talk Recording: https://youtu.be/6cWquAEqKw4

**Abstract**: The inevitable accumulation of errors in near-future quantum devices represents a key obstacle in delivering practical quantum advantage. This motivated the development of various quantum error-mitigation protocols, each representing a method to extract useful computational output by combining measurement data from multiple samplings of the available imperfect quantum device. What are the ultimate performance limits universally imposed on such protocols? Here, we derive a fundamental bound on the sampling overhead that applies to a general class of error-mitigation protocols, assuming only the laws of quantum mechanics. We use it to show that (1) the sampling overhead to mitigate local depolarizing noise for layered circuits --- such as the ones used for variational quantum algorithms --- must scale exponentially with circuit depth, and (2) the optimality of probabilistic error cancellation method among all strategies in mitigating a certain class of noise, demonstrating that our results provide a means to identify when a given quantum error-mitigation strategy is optimal and when there is potential room for improvement.

**Bio**: I am a postdoctoral research fellow at Nanyang Technological University in Singapore. Topics I have worked on include quantum resource theories, quantum communication, continuous-variable quantum information, and quantum error correction and mitigation. I particularly contributed to developing general frameworks of quantum resource theories and recently have applied these insights into the performance analysis of quantum error mitigation protocols. Prior to the current position, I was a PhD student at MIT supervised by Seth Lloyd.

<img src="/images/rt.jpg" width="200px">

## 16:00 – 16:50 (UTC): Eduardo Miranda - **Developing a Musical Instrument using a Photonic Quantum Computer**
Talk Recording: https://youtu.be/ihOFT9aYjeI

**Abstract**: In this talk we report on the initial results from our research into developing electronic musical instruments using photonic quantum computers. Specifically, we introduce three experimental sound synthesisers that render audio following the preparation, manipulation, and measurement of photons in a process known as Gaussian Boson Sampling (GBS). The synthesisers are based on two types of architectures: additive synthesis and granular sampling. The additive architecture employs a bank of oscillators, each of which generates a sinusoidal wave. Then, the waves are added to produce the resulting sound. Sinusoidal waves are characterized by their respective amplitudes, frequencies and phases. Varying these characteristics will yield perceptible differences in the timbre of the resulting sound. In contrast, the granular sampling architecture produces sounds by cutting short sections from a given sound and splicing them together to form a new one.

**Bio**: Eduardo R. Miranda is a professor in computer music at the University of Plymouth, UK, where he leads the Interdisciplinary Centre for Computer Music Research. Eduardo and his team are pioneering research into quantum computing applied to musical creativity, thanks to a research grant from UK's QCS Hub. His book 'Handbook on Artificial Intelligence for Music' has recently been published by Springer.

<img src="/images/em.png" width="170px">

## 16:50 – 17:20 (UTC): Réka Deák & Aurél Gábris - **Quing**
Talk Recording: https://youtu.be/07QBh1cR1Pk

Watch "Quing Solomon" short film: https://cqtshorts.quantumlah.org/entry/quing-solomon

**Abstract**: We are going to present our devising process leading to an artwork centered around quantum physics. The creative team consists of a scientist, a media artist, a fine artist and two puppeteers.  By showing our journey we are hoping to prove that the gap between the Quantum and Artistic world is not as wide as it seems and that these two can go hand in hand.

**Bio**: *Réka Deák* is a Hungarian puppeteer, stage designer, theatre and film director. She studied stage design at DAMU / Czechia. She is a PhD student at Charles University in Prague. She is a co-founder of  Dafa Puppet Theatre. She worked as a designer, puppeteer and drama pedagogue in Europe, Arab world and Asia. Her short film Quing Solomon was shortlisted at Quantum Shorts 2021 in Singapore.

<img src="/images/rd.jpg" width="170px">

**Bio**: *Aurél Gábris* is a researcher at the Czech Technical University in Prague and at the Wigner Research Centre for Physics in Budapest. He is particularly fascinated by the concept of measurement in quantum mechanics, and has devoted much of his research into understanding the implications and possible applications for quantum computing and simulation. His vision is to work towards sharing this fascination with a wide audience through expressions of art, as well as one of the coordinators of the QEducation Department of QWorld.

<img src="/images/agg.jpeg" width="160px">

## 17:20 - 17:40 (UTC): Adam Fattal - **haq.ai - A Quantum Programming Challenge Platform**
Talk Recording: https://youtu.be/h-XX6V1vlc0

Visit the platform: https://www.haq.ai/

**Bio**: I'm an undergraduate of physics at Durham University
<br><br>

# Thursday, Nov 25

## 17:00 – 18:00 (UTC): Nicole Yunger Halpern - **Quantum steampunk: Quantum information meets thermodynamics**
NIST, QuICS, University of Maryland

Talk Recording: https://youtu.be/UVSNRy3F0ew

**Abstract**: Thermodynamics has shed light on engines, efficiency, and time’s arrow since the Industrial Revolution. But the steam engines that powered the Industrial Revolution were large and classical. Much of today’s technology and experiments are small-scale, quantum, far from equilibrium, and processing information. Nineteenth-century thermodynamics requires updating for the 21st century. Guidance has come from the mathematical toolkit of quantum information theory. Applying quantum information theory to thermodynamics sheds light on fundamental questions (e.g, how does entanglement spread during quantum thermalization? How can we distinguish quantum heat from quantum work?) and practicalities (e.g., nanoscale engines and the thermodynamic value of quantum coherences). I will overview how quantum information theory is being used to modernize thermodynamics for  quantum-information-processing technologies. I call this combination quantum steampunk, after the steampunk genre of literature, art, and cinema that juxtaposes futuristic technologies with 19th-century settings.

**Bio**: Nicole Yunger Halpern is a physicist at the National Institute of Science and Technology (NIST), a QuICS Fellow at the Joint Institute for Quantum Information and Computer Science (QuICS), a JQI Affiliate at the Joint Quantum Institute (JQI), and an Adjunct Professor of Physics and of the Institute for Physical Science and Technology (IPST) at the University of Maryland.

Nicole completed her physics PhD in 2018, under John Preskill’s auspices at Caltech. Her dissertation won the Ilya Prigogine Prize for a thermodynamics PhD thesis. In 2020, she received the International Quantum Technology Emerging Researcher Award from the Institute of Physics. Nicole earned her Masters at the Perimeter Scholars International (PSI) program of the Perimeter Institute for Theoretical Physics. Before that, she earned her Bachelors at Dartmouth College, where she graduated as a co-valedictorian of her class.

<img src="/images/nyh.jpeg" width="170px">

## 18:00 – 18:30 (UTC): Xavier Coiteux-Roy - **An environment to evolve collective explanations**
Talk Recording: https://youtu.be/MpgHSbndExo

**Abstract**: Scientific ideas evolve through conjecture and critique. This evolution was greatly accelerated by the advent of the Internet. Yet, academia is rather conservative in its use of the internet — a lot still happens offline. I will re-visit the classic idea of an online platform to open the scientific discussion. I will conjecture some key features of such open-science platform. I will also discuss where many similar projects have failed — namely because of the publish or perish problem — and offer a tentative path to circumvent it. In a Popperian spirit, critiques are very welcome.

**Bio**: Xavier Coiteux-Roy is doing his PhD in informatics at the Università della Svizzera italiana in Switzerland. While enthusiastic about fundamental science, his usual work is at the frontier of quantum information, thermodynamics, and cryptography. In his free time, Xavier is most often found climbing at the gym or hiking in the mountains.

<img src="/images/xcr.jpg" width="160px">

<br><br>

